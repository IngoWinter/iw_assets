Redaxo Addon iw_assets
======================

iw_assets kombiniert [lessphp](https://github.com/leafo/lessphp) und [minify](https://github.com/leafo/lessphp).
CSS und Javascript Dateien werden zusammengefasst und als eine Cachedatei gespeichert. Ändern sich die Quelldateien, wird automatisch die Cachedatei erneuert um umbenannt. Dadurch lassen sich per .htaccess sehr lange Cachezeiten für die Ressourcen einstellen:

    ######## GN2 https://bitbucket.org/gn2netwerk/processor ###############################
    # Additional processor rules
    <IfModule mod_headers.c>
        Header unset ETag
    </IfModule>

    FileETag None

    # compress javascript, css:
	AddOutputFilterByType DEFLATE text/plain
	AddOutputFilterByType DEFLATE text/html
	AddOutputFilterByType DEFLATE image/gif
	AddOutputFilterByType DEFLATE image/png
	AddOutputFilterByType DEFLATE image/jpg
	AddOutputFilterByType DEFLATE image/jpeg
	AddOutputFilterByType DEFLATE image/pjpeg
	AddOutputFilterByType DEFLATE text/xml
	AddOutputFilterByType DEFLATE text/css
	AddOutputFilterByType DEFLATE text/javascript
	AddOutputFilterByType DEFLATE application/rss+xml
	AddOutputFilterByType DEFLATE application/javascript
	AddOutputFilterByType DEFLATE application/x-javascript

    <FilesMatch "\\.(js|css|html|htm|php|xml)$">
      SetOutputFilter DEFLATE
    </FilesMatch>

    <IfModule mod_expires.c>
        ExpiresActive on
        ExpiresDefault                                      "access plus 1 month"
        ExpiresByType text/css                              "access plus 1 year"
        ExpiresByType application/json                      "access plus 0 seconds"
        ExpiresByType application/xml                       "access plus 0 seconds"
        ExpiresByType text/xml                              "access plus 0 seconds"
        ExpiresByType image/x-icon                          "access plus 1 week"
        ExpiresByType text/x-component                      "access plus 1 month"
        ExpiresByType text/html                             "access plus 0 seconds"
        ExpiresByType application/javascript                "access plus 1 year"
        ExpiresByType application/x-web-app-manifest+json   "access plus 0 seconds"
        ExpiresByType text/cache-manifest                   "access plus 0 seconds"
        ExpiresByType audio/ogg                             "access plus 1 month"
        ExpiresByType image/gif                             "access plus 1 month"
        ExpiresByType image/jpeg                            "access plus 1 month"
        ExpiresByType image/png                             "access plus 1 month"
        ExpiresByType video/mp4                             "access plus 1 month"
        ExpiresByType video/ogg                             "access plus 1 month"
        ExpiresByType video/webm                            "access plus 1 month"
        ExpiresByType application/atom+xml                  "access plus 1 hour"
        ExpiresByType application/rss+xml                   "access plus 1 hour"
        ExpiresByType application/font-woff                 "access plus 1 month"
        ExpiresByType application/vnd.ms-fontobject         "access plus 1 month"
        ExpiresByType application/x-font-ttf                "access plus 1 month"
        ExpiresByType font/opentype                         "access plus 1 month"
        ExpiresByType image/svg+xml                         "access plus 1 month"
    </IfModule>
    ######## /GN2 ###############################

Installation
------------

Addon herunterladen und wie alle anderen Redaxo Addons installieren. WICHTIG: das oberste Addon Verzeichnis muss "iw_assets" heissen.

Schnelleinstieg
---------------

Im Template ein iw_assets Objekt erstellen und die Pfade zu den Dateien angeben:

    $assets = new iw_assets();
    $assets->add('layout/normalize.css');
    $assets->add('layout/structure.css');
    $assets->add('layout/base.less');
    $assets->add('layout/jquery.js');
    $assets->add('layout/standard.js');
    $assets->add('layout/jquery.cycle.lite.js');

Dann an der gewünschten Stelle die kombinierten Dateien ausgeben:

    echo $assets->get_css();
    echo $assets->get_js();

Gruppen
-------

Bei Bedarf können Gruppen definiert werden, falls z.B. ein Teil der Skripte im `<head>` und ein anderer Teil vor `</body>` ausgegeben werden soll:

    $assets->add('layout/jquery.js', 'head');
    $assets->add('layout/standard.js', 'head');
    $assets->add('layout/jquery.cycle.lite.js', 'body');

    echo $assets->get_js('head');
    ...
    echo $assets->get_js('body');

.less Dateien
-------------

.less Dateien werden über lessphp in .css Dateien umgewandelt. Die .css Datei wird im selben Verzeichnis wie die .less Datei gespeichert. Zusätzlich wird auch noch eine .cache Datei erstellt und gespeichert. Diese ist nötig um weitere über @import eingebundene .less Dateien automatisch kompilieren zu können.

Methoden
--------

    $assets->add($filepath, $groupname = 'default');

Fügt eine Datei zu einer Gruppe hinzu. Wird keine Gruppe angegeben, wird automatisch zur Gruppe 'default' hinzugefügt.

    echo $assets->get_css($groupname = 'default', $mediatype = 'all');
    echo $assets->get_js($groupname = 'default');

Gibt für die Dateien der Gruppe eine `<link>` / `<script>` Referenz aus.

    $assets = new iw_assets();
    $assets->debug();
    ...

Aktiviert den debug-Modus, für jede Datei wird eine einzelne `<link>` / `<script>` Referenz ausgegeben.