<?php
$mypage = 'iw_assets';
$myroot = $REX['INCLUDE_PATH'].'/addons/'.$mypage.'/';

require $myroot.'classes/class.iw_assets.inc.php';
require $myroot.'vendor/lessphp/lessc.inc.php';
require $myroot.'vendor/minify/min/lib/Minify/Loader.php';
Minify_Loader::register();
?>