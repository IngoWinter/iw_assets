<?php
class iw_assets {

	protected $groups = array();
	protected $admin = false;
	protected $debug = false;
	protected $cachepath = '/files/addons/iw_assets/';


	public static function factory()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new iw_assets();
		}
		return $inst;
	}

	/*
	 * checks if user is logged in admin, only then assets will be processed
	 * $force_admin_mode is useful for development on multidomain installations
	 */
	public function __construct ($force_admin_mode = false) {
		global $REX;
		$loggedIn = isset($_SESSION[$REX['INSTNAME']]['UID']) && $_SESSION[$REX['INSTNAME']]['UID'] > 0;
		if ($loggedIn && (!isset($REX['LOGIN']) || !is_object($REX['LOGIN']))) {
			$REX['LOGIN'] = new rex_backend_login($REX['TABLE_PREFIX'] . 'user');
			$loggedIn = $REX['LOGIN']->checkLogin();
		}
		if ($loggedIn && $REX['LOGIN']->USER->isAdmin() || $force_admin_mode)
		{
			$this->admin = true;
		}
	}


	/*
	 * activates debug mode (files will not be minified)
	 */
	public function debug ()
	{
		$this->debug = true;
	}


	/*
	 * adds assets
	 *
	 * @param $filepath: complete path to file STRING
	 * @param $groupname optional STRING
	 */
	public function add ($filepath, $groupname = 'default')
	{
		if ($this->check_filepath($filepath))
		{
			if ($groupname != 'default')
			{
				$this->check_groupname($groupname);
			}
			$ext = substr($filepath, (strrpos($filepath, '.') + 1));
			if ($ext === 'less')
			{
				if($this->admin)
				{
					$this->compile_less($this->get_abs_filepath($filepath), str_replace('.less', '.css', $this->get_abs_filepath($filepath)));
				}
				$filepath = str_replace('.less', '.css', $filepath);
				$ext = 'css';
			}
			$this->groups[$groupname][$ext][] = $filepath;
		}
	}


	/*
	 * returns <link> reference(s) for a group
	 *
	 * @param $groupname STRING
	 * @param $mediatype: eg "all"/"screen"/"print" STRING
	 */
	public function get_css ($groupname = 'default', $mediatype = 'all')
	{
		if (isset($this->groups[$groupname])
			&& isset($this->groups[$groupname]['css'])
			&& count($this->groups[$groupname]['css']))
		{
			if ($this->debug)
			{
				$debug = '';
				foreach ($this->groups[$groupname]['css'] as $filepath)
				{
					$debug .= '<link rel="stylesheet" href="'.$filepath.'" type="text/css" media="'.$mediatype.'" />'."\n";
				}
				return $debug;
			}
			$cachefile = $this->get_cachefile($groupname, 'css');
			return '<link rel="stylesheet" href="'.$this->cachepath.$cachefile.'" type="text/css" media="'.$mediatype.'" />';
		}
	}


	/*
	 * returns <script> reference for a group
	 *
	 * @param $groupname STRING
	 */
	public function get_js ($groupname = 'default')
	{
		if (isset($this->groups[$groupname])
			&& isset($this->groups[$groupname]['js'])
			&& count($this->groups[$groupname]['js']))
		{
			if ($this->debug)
			{
				$debug = '';
				foreach ($this->groups[$groupname]['js'] as $filepath)
				{
					$debug .= '<script src="'.$filepath.'"></script>'."\n";
				}
				return $debug;
			}
			$cachefile = $this->get_cachefile($groupname, 'js');
			return '<script src="'.$this->cachepath.$cachefile.'"></script>';
		}
	}


	/*
	 * returns filename of cachefile and generates cachefile if necessary
	 * checks if assets have changed (.manifest file)
	 *
	 * @param $groupname STRING
	 * @param $ext STRING
	 */
	protected function get_cachefile ($groupname, $ext)
	{
		$filepaths = array();
		$cachefile = $groupname.time();
		foreach ($this->groups[$groupname][$ext] as $filepath)
		{
			$filepaths[] = $this->get_abs_filepath($filepath);
		}
		$cache = (glob($this->get_abs_filepath($this->cachepath.$groupname.'??????????.'.$ext)));
		if ($this->admin)
		{
			$manifest = (glob($this->get_abs_filepath($this->cachepath.$groupname.'.'.$ext.'.manifest')));
			if (!count($manifest))
			{
				file_put_contents($this->get_abs_filepath($this->cachepath.$groupname.'.'.$ext.'.manifest'), serialize($filepaths));
			}
			else
			{
				if (unserialize(file_get_contents($manifest[0])) != $filepaths)
				{
					foreach ($cache as $file)
					{
						unlink($file);
					}
					$cache = array();
					file_put_contents($this->get_abs_filepath($this->cachepath.$groupname.'.'.$ext.'.manifest'), serialize($filepaths));
				}
			}
		}
		if (!count($cache))
		{
			file_put_contents($this->get_abs_filepath($this->cachepath.$cachefile.'.'.$ext), Minify::combine($filepaths));
		}
		elseif (count($cache) === 1)
		{
			if ($this->admin)
			{
				$cachefile_age = filemtime($cache[0]);
				$cachefile_created = false;
				foreach ($this->groups[$groupname][$ext] as $file)
				{
					if (filemtime($this->get_abs_filepath($file)) > $cachefile_age)
					{
						unlink($cache[0]);
						file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->cachepath.$cachefile.'.'.$ext, Minify::combine($filepaths));
						$cachefile_created = true;
						break;
					}
				}
				if (!$cachefile_created)
				{
					$cachefile = basename($cache[0], '.'.$ext);
				}
			}
			else
			{
				$cachefile = basename($cache[0], '.'.$ext);
			}
		}
		else
		{
			foreach ($cache as $file)
			{
				unlink($file);
			}
			file_put_contents($this->get_abs_filepath($this->cachepath.$cachefile.'.'.$ext), Minify::combine($filepaths));
		}
		return $cachefile.'.'.$ext;
	}


	/*
	 * checks if groupname is valid
	 *
	 * @param $groupname STRING
	 */
	protected function check_groupname ($groupname)
	{
		if (!preg_match('/^[A-Za-z0-9_-]+$/', $groupname))
		{
			die('iw_assets: der Gruppenname "'.$groupname.'" enthält nicht erlaubte Zeichen. Erlaubt sind "A-Z", "a-z", "0-9", "_" und "-".');
		}
	}


	/*
	 * checks if filepath is valid
	 *
	 * @param $filepath STRING
	 */
	protected function check_filepath ($filepath)
	{
		if ($this->admin && !is_file($this->get_abs_filepath($filepath)))
		{
			error_log('iw_assets: '.$filepath.' ist nicht vorhanden.');
			return false;
		}
		return true;
	}


	/*
	 * returns absolute filepath
	 *
	 * @param $filepath STRING
	 */
	protected function get_abs_filepath ($filepath)
	{
		return preg_replace('/\/+/', '/', $_SERVER['DOCUMENT_ROOT'].'/'.$filepath);
	}


	/*
	 * cachedCompile() autocompiles .less files form @import. to recognize changes
	 * in files from @import a cache file is needed
	 *
	 * @param $inputFile STRING
	 * @param $outputFile STRING
	 */
	protected function compile_less ($inputFile, $outputFile) {
		$cacheFile = $inputFile.'.cache';
		if (file_exists($cacheFile)) {
			$cache = unserialize(file_get_contents($cacheFile));
			foreach ($cache['files'] as $k => $v)
			{
				if (!is_file($k))
				{
					unlink($inputFile.'.cache');
					$cache = $inputFile;
					break;
				}
			}
		} else {
			$cache = $inputFile;
		}
		$less = new lessc;
		$newCache = $less->cachedCompile($cache);
		if (!is_array($cache) || $newCache['updated'] > $cache['updated']) {
			file_put_contents($cacheFile, serialize($newCache));
			file_put_contents($outputFile, $newCache['compiled']);
		}
	}

}
?>